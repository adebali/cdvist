celery==3.1.21
redis==2.10.5
flower==0.8.4
flask_debugtoolbar
simplejson
Interval==1.0.0