import os
import sys
import commentjson as json

scriptDirectory = os.path.dirname(__file__)

with open(scriptDirectory + '/config.json', 'r') as f:
    config = json.load(f)

projectRoot = os.path.join(os.path.dirname(__file__), '../..')
scriptsDir = os.path.join(projectRoot, '../vendor')

def run(l):
    os.system(os.system(' '.join(l)))

def installBlast():
    script = os.path.join(scriptsDir, 'installLBlast.sh')
    version = config['vendor']['version']['tools']['blast']
    targetDir = projectRoot + config['vendor']['target_directory']['tools']['blast']
    run(['bash', script, version, targetDir])
    

def installHmmer():
    script = os.path.join(scriptsDir, 'install-hmmer3.sh')
    version = config['vendor']['version']['tools']['hmmer3']
    targetDir = projectRoot + config['vendor']['target_directory']['tools']['hmmer3']
    run(['bash', script, version, targetDir])
    

def downloadPfam():
    script = os.path.join(scriptsDir, 'download-pfam-hmm.sh')
    version = config['vendor']['version']['databases']['pfam']
    targetDir = projectRoot + config['vendor']['target_directory']['databases']['pfam']
    run(['bash', script, version, targetDir])
    

def downloadHHsuiteDB(hhsuiteDatabase):
    script = os.path.join(scriptsDir, 'download-hh-suite-hhm.sh')
    dbSpecificObject = config['vendor']['version']['databases']['hh-suite'][hhsuiteDatabase]
    remoteDir = dbSpecificObject['remoteDir']
    version = dbSpecificObject['name'] + dbSpecificObject['extension']
    targetDir = projectRoot + config['vendor']['target_directory']['databases']['hh-suite']
    run(['bash', script, remoteDir, version, targetDir])
    

# def installHHsuite():
#     script = scriptRoot + 'install-hh-suite.sh'
#     version = config['vendor']['version']['tools']['hh-suite']
#     targetDir = projectRoot + config['vendor']['target_directory']['tools']['hhsuite']
#     run(['bash', script, remoteDir, version, targetDir])
#     

def installCoils():
    command = os.path.join(scriptsDir, 'install-coils.sh')
    os.system('bash ' + command)

def installSeg():
    command = os.path.join(scriptsDir, 'install-seg.sh')
    os.system('bash ' + command)

def main():
    print('main')
    # installBlast()
    downloadPfam()
    # downloadHHsuiteDB('pfam')
    # downloadHHsuiteDB('pdb')
    # downloadHHsuiteDB('uniprot')
    # downloadHHsuiteDB('scop')


if __name__=='__main__':
    main()