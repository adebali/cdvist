#!/bin/bash

set -e

VERSION=$1

if [[ -z "$VERSION" || -z "$TARGET_DIR" ]]; then
	echo "Usage: $0 <blast version> <target directory>"
	exit 1
fi

TARGET_VERSION_DIR=/home/ogun/cdvist/flask-app/vendor/tools/blast/$VERSION
mkdir -p $TARGET_VERSION_DIR

if [[ -s "$TARGET_VERSION_DIR/rpsblast" ]]; then
	echo "Blast version $VERSION is already installed"
	exit
fi

TARBALL_BASENAME="ncbi-blast-${VERSION}+-src"
TARBALL_FILENAME="ncbi-blast-${VERSION}+-src.tar.gz"
BLAST_URL="ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/$VERSION/$TARBALL_FILENAME"
echo "Downloading BLAST tarball"
cd /tmp
wget --no-verbose $BLAST_URL
echo "Decompressing tarball"
tar zxvf $TARBALL_FILENAME
cd $TARBALL_BASENAME/c++
echo "Configuring BLAST Suite"
./configure --with-mt --prefix=$TARGET_VERSION_DIR --without-debug
echo "Installing BLAST Suite"
make
make install

echo "Cleaning up"
cd /tmp
rm -r $TARBALL_BASENAME
rm $TARBALL_FILENAME

echo "Successfully installed BLAST version $VERSION"
