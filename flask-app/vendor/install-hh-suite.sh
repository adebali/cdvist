#!/bin/bash

VERSION=$1
TARGET_DIR=$2

if [[ -z "$VERSION" || -z "$TARGET_DIR" ]]; then
	echo "Usage: $0 <version> <target directory>"
	exit 1
fi

cd $TARGET_DIR

if [[ -s "$VERSION/bin/hhsearch" &&
	-s "$VERSION/bin/hhblits" ]]; then
	echo "HHsuite $VERSION is already installed"
	exit
fi
rm -rf $VERSION


wget -O hhsuite.tar.gz https://github.com/soedinglab/hh-suite/releases/download/$VERSION/hhsuite-${VERSION#"v"}-Linux.tar.gz
tar xzvf hhsuite.tar.gz -C ./
mv hhsuite-${VERSION#"v"}-Linux $VERSION
rm hhsuite.tar.gz
