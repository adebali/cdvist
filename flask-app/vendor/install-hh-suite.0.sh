#!/bin/bash


TARGET_DIR=$1

if [[ -z "$VERSION" || -z "$TARGET_DIR" ]]; then
	echo "Usage: $0 <pfam version> <target directory>"
	exit 1
fi

if [ $VERSION = "3" ]; then
    git clone https://github.com/soedinglab/hh-suite.git $TARGET_DIR
    cd $TARGET_DIR
    git pull
    git submodule deinit
    git submodule init

    git clone https://github.com/soedinglab/ffindex_soedinglab.git $TARGET_DIR/lib/ffindex
    exit 

    mkdir -p build
    cd build
    cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=${INSTALL_BASE_DIR} ..
    cd ../
    make
    sudo make install
    sudo rm /README
    sudo rm /LICENCE
    sudo rm /hhsuite*.pdf

    export HHLIB=${INSTALL_BASE_DIR}
    export PATH=${PATH}:${INSTALL_BASE_DIR}/bin:${INSTALL_BASE_DIR}/scripts
    exit
fi
