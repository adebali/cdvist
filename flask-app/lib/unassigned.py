#!/usr/bin/env python

from interval import Interval, IntervalSet

def domain2interval(domain):
    return Interval(domain['start'], domain['end'])

def domains2intervalSet(domains):
    intervals = IntervalSet()
    for domain in domains:
        intervals.add(domain2interval(domain))
    return intervals

def intervals2unassignedSegments(intervals, intervalLengthCutoff = 0):
    unassignedSegments = []
    for interval in intervals:
        intervalLength = interval.upper_bound - interval.lower_bound + 1
        if intervalLength > intervalLengthCutoff:
            unassignedSegments.append({
                'start': interval.lower_bound,
                'end': interval.upper_bound
            })
    return unassignedSegments

def getUnassignedSegments(proteinObject, gapLength = 0):
    length = proteinObject['length']
    domains = proteinObject['segments']['assigned']
    proteinInterval = IntervalSet([Interval(1, length)])
    domainIntervals = domains2intervalSet(domains)
    unassignedIntervals = proteinInterval - domainIntervals
    unassignedSegments = intervals2unassignedSegments(unassignedIntervals, gapLength)
    return unassignedSegments

def relativeIntervalTransform(originalInterval, subInterval):
    originalLowerBound = originalInterval.lower_bound
    absInterval = Interval(originalLowerBound + subInterval.lower_bound, originalLowerBound + subInterval.upper_bound)
    return absInterval

def relativeDomainTransform(originalDomain, subDomain):
    start1 = originalDomain['start']
    end1 = originalDomain['end']
    start2 = subDomain['start']
    end2 = subDomain['end']
    transformedInterval = relativeIntervalTransform(Interval(start1, end1), Interval(start2, end2))
    newDomain = subDomain
    newDomain['start'] = transformedInterval.lower_bound
    newDomain['end'] = transformedInterval.upper_bound
    return newDomain